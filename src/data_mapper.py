from .db_context import IDbContext


class Product:
    def __init__(self, fullname: str, age: int):
        self.fullname = fullname
        self.age = age

    def some_business_logic(self):
        # do stuff
        pass


class ProductDataMapper:
    def __init__(self, db_context: IDbContext):
        self._db_context = db_context

    def save(self, product: Product) -> None:
        sql_string = f"INSERT INTO app_product (fullname, age) values ({product.fullname}, {product.age})"
        self._db_context.execute_sql(sql=sql_string)
