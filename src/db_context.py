import abc
from typing import NewType

SqlResult = NewType('SqlResult')


class IDbContext(abc.ABC):
    @abc.abstractmethod
    def execute_sql(self, sql: str) -> SqlResult:
        raise NotImplementedError
